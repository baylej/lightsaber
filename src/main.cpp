#include "model.hpp"
#include "viewer.hpp"
#include "saber.hpp"

#include <opencv2/opencv.hpp>

using namespace cv;

std::vector<Saber*> *sabers;

struct _config globConfig;

int main(int argc, char *argv[])
{	
    VideoCapture camera(0); // Opens the default camera.
    if(!camera.isOpened()) return -1;
	
	sabers = new std::vector<Saber*>();
	
	globConfig.deltaH = 30;
	globConfig.deltaS = globConfig.deltaV = 60;
	globConfig.maxLineGap = 15;
	globConfig.minLineLength = 30;
	globConfig.threshold = 180;
	globConfig.rho = 1;
	globConfig.theta = 32;
	
	Saber2d s2d(&camera); // Spawns a new instance of the Saber2d OpenGL App.
	Viewer::mainStart(&s2d); // Create a QT viewer to display our Saber2d App.
	
	for(std::vector<Saber*>::iterator i=sabers->begin(); i!=sabers->end(); i++) {
		delete *i;
	}
	delete sabers;
	
	return 0;
}
