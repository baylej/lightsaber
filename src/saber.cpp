#include "saber.hpp"
#include "data.hpp"

#include <iostream>
#include <cstring>
#include <math.h>

#include <QImage>

using std::memset;
using std::cerr;
using std::endl;

using namespace cv;

static QImage saber_tex;

Saber2d::Saber2d(VideoCapture *camera): winHeight(0), winWidth(0) {
	this->camera = camera;
	if (saber_tex.isNull()) {
		saber_tex.loadFromData(saber_tex_data, saber_tex_len, "PNG");
	}
}

void Saber2d::initialize() {
	// Initialise the OpenGL context.
	GLApp2d::initialize();
	
	// Generate a named texture to load the saber texture into the RAM of the GPU.
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 4, saber_tex.width(), saber_tex.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, saber_tex.bits());
	
	glBindTexture(GL_TEXTURE_2D, texture);
	
	GLERRCHK;
}

static int distance(int xa, int ya, int xb, int yb){
	return (int) sqrt((xb-xa)*(xb-xa) + (yb-ya)*(yb-ya));
}

void Saber2d::render() {
	if (camera != NULL && camera->isOpened()) {
		GLfloat coord[16];
		memset(coord, 0, 16*sizeof(float));
		
		Mat frame;
		camera->read(frame);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, frame.cols, frame.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, frame.data);
		
		// Clears the screen.
		glClearColor(0.0, 0.0, 0.0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT);
		
		// The coord matrix is like the model matrix, it transforms the coordinates of the vertices of the backplane.
		coord[0]  = frame.cols/2.;
		coord[5]  = frame.rows/2.;
		coord[10] = coord[15] = 1.;
		coord[12] = ( winWidth-frame.cols) / 2. + frame.cols/2.;
		coord[13] = (winHeight-frame.rows) / 2. + frame.rows/2.;
		glUniformMatrix4fv(coord_m4, 1, GL_FALSE, coord);
		
		// Calls super-class render method to draw the frame.
		GLApp2d::render();
		
		// Render of the sabers.
		// Convert the captured frame from BGR to HSV.
		Mat frameHSV;
		cv::cvtColor(frame, frameHSV, COLOR_BGR2HSV);
		
		// Translation applied to the frame when drawn on screen
		float xtr = ( winWidth-frame.cols) / 2.;
		float ytr = (winHeight-frame.rows) / 2.;
		
		// For each saber.
		for(std::vector<Saber*>::iterator i=sabers->begin(); i!=sabers->end(); i++) {
			// Detect all the segments.
			std::vector<cv::Vec4i> *lines = detectSaber(frameHSV, *i);
			pos p_max, p_min, pos_fin;
			int distmax, distmin, sizeSaber;
			p_max.x1 = -1;
			// For each segment.
			for(std::vector<cv::Vec4i>::iterator j=lines->begin(); j!=lines->end(); j++) {
				cv::Vec4i vec = *j;
				if(vec[0] < vec[2]){
					int tmpx1 = vec[0], tmpy1 = vec[1];
					vec[0] = vec[2]; vec[1] = vec[3];
					vec[2] = tmpx1; vec[3] = tmpy1;
				}
				if(p_max.x1 == -1){
					p_max.x1 = vec[0]+xtr;   p_max.x2 = vec[2]+xtr;
					p_max.y1 = vec[1]+ytr;   p_max.y2 = vec[3]+ytr;
					p_min.x1 = vec[0]+xtr;   p_min.x2 = vec[2]+xtr;
					p_min.y1 = vec[1]+ytr;   p_min.y2 = vec[3]+ytr;
						
				}
				else {
					if(p_max.x1 < vec[0]+xtr){ // valeur max en x, on sauvegarde le point
						p_max.x1 = vec[0]+xtr;	p_max.y1 = vec[1]+ytr;
					}
					if(p_max.y2 < vec[1]+xtr){ // valeur max en y, on sauvegarde le point
					   p_max.x2 = vec[0]+xtr;   p_max.y2 = vec[1]+ytr;
					}
					if(p_min.x1 > vec[2]+xtr){ // valeur min en x, on sauvegarde le point
					   p_min.x1 = vec[2]+xtr;   p_min.y1 = vec[3]+ytr;
					}
					if(p_min.y2 > vec[3]+xtr){ // valeur min en y, on sauvegarde le point
					   p_min.x2 = vec[2]+xtr;   p_min.y2 = vec[3]+ytr;
					}
				}
			}
			//distance entre les deux points max
			distmax = distance(p_max.x2, p_max.y2, p_max.x1, p_max.y1);
			// distance entre un point max et son adjacent min
			distmin = distance(p_max.x2, p_max.y2, p_min.x1, p_min.y1);
			/* 
			 * calcul des deux points centraux et de la largeur
			 */
			if(distmax < distmin){
				sizeSaber = distmax/2;
				pos_fin.x1 = (p_max.x1 + p_max.x2)/2.;
				pos_fin.y1 = (p_max.y1 + p_max.y2)/2.;

				pos_fin.x2 = (p_min.x1 + p_min.x1)/2.;
				pos_fin.y2 = (p_min.y1 + p_min.y1)/2.;
			}
			else {
				sizeSaber = distmin/2;
				pos_fin.x1 = (p_max.x1 + p_min.x2)/2.;
				pos_fin.y1 = (p_max.y1 + p_min.y2)/2.;

				pos_fin.x2 = (p_min.x1 + p_max.x2)/2.;
				pos_fin.y2 = (p_min.y1 + p_max.y2)/2.;
			}
			// Render as a saber.
			if(!(distmin < 2 || distmax < 2))
				renderSaber(**i, pos_fin, sizeSaber);
		}
		
		GLERRCHK;
	}
}

void Saber2d::resize(int width, int height) {
	this->winHeight = height;
	this->winWidth  =  width;
	
	GLApp2d::resize(width, height);
}

void Saber2d::renderSaber(Saber &saber, pos &pos, int sizeSaber) {
	// Color of the saber.
	glUniform4f(blend_col_v4, saber.saberColor.r/255., saber.saberColor.g/255., saber.saberColor.b/255., 1.);
	
	// Send the vertex coordinates through the coord_m4 matrix in mode 2
	float coord[16];
	memset(coord, 0, 16*sizeof(float));
	coord[0] = pos.x1-sizeSaber;  coord[4] = pos.x1+sizeSaber;
	coord[8] = pos.x2+sizeSaber;  coord[12] = pos.x2-sizeSaber;
	
	coord[1] = pos.y1;  coord[5] = pos.y1;
	coord[9] = pos.y2;  coord[13] = pos.y2;

	coord[3] = coord[7] = coord[11] = coord[15] = 1.;
	glUniformMatrix4fv(coord_m4, 1, GL_FALSE, coord);
	glUniform1i(mode_i, 2);
	
	glBindTexture(GL_TEXTURE_2D, texture_id);
	
	GLApp2d::render();
	
	// Cleans the uniforms.
	glUniform1i(mode_i, 1);
	glUniform4f(blend_col_v4, 1., 1., 1., 1.);
	glBindTexture(GL_TEXTURE_2D, texture);
}

// convert RGB color to HSV
// see http://docs.opencv.org/modules/imgproc/doc/miscellaneous_transformations.html#cvtcolor
static col RGB2HSV(col &rgb) {
	col res;
	
	float rn, gn, bn;
	rn = rgb.r / 255.;
	gn = rgb.g / 255.;
	bn = rgb.b / 255.;
	
	float max_v, min_v;
	max_v = (rn > bn && rn > gn) ? rn : ((gn > bn) ? gn : bn);
	min_v = (rn < bn && rn < gn) ? rn : ((gn < bn) ? gn : bn);
	
	float V = max_v;
	
	float S = 0.;
	if (V != 0.)
		S = (V - min_v) / V;
	
	float D = 1.;
	if (max_v - min_v != 0)
		D = (max_v - min_v);
	
	float H;
	if (max_v == rn) 
		H = 60 * (gn - bn) / D;
	else if (max_v == gn)
		H = 120 + 60 * (bn - rn) / D;
	else
		H = 240 + 60 * (rn - gn) / D;
	if (H < 0)
		H += 360;
	
	res.r = (int)(H/2.);
	res.g = (int)(255*S);
	res.b = (int)(255*V);
	
	return res;
}

std::vector<cv::Vec4i>* Saber2d::detectSaber(Mat &frameHSV, Saber *saber) {
	// TODO
	// http://opencv-srf.blogspot.ro/2010/09/object-detection-using-color-seperation.html
	col hsv = RGB2HSV(saber->detectColor);
	
	Mat frameBin;
	
	// Threshold the image to make a binary image.
	cv::inRange(frameHSV, cv::Scalar(hsv.r-globConfig.deltaH, hsv.g-globConfig.deltaS, hsv.b-globConfig.deltaV),
	                      cv::Scalar(hsv.r+globConfig.deltaH, hsv.g+globConfig.deltaS, hsv.b+globConfig.deltaV), frameBin);
	
	// Use the Hough lines detector to get the position of the saber for the current frame.
	std::vector<cv::Vec4i> *lines = new std::vector<cv::Vec4i>();
	cv::HoughLinesP(frameBin, *lines, globConfig.rho, CV_PI/(float)globConfig.theta, globConfig.threshold, globConfig.minLineLength, globConfig.maxLineGap);
	
	return lines;
}
