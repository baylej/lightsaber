#include "qtmodeladapter.hpp"

int QtModelAdapter::rowCount(const QModelIndex &parent) const {
	return sabers->size();
}

QVariant QtModelAdapter::data(const QModelIndex &index, int role) const {
	if (role == Qt::DisplayRole) {
		Saber *s = (*sabers)[index.row()];
		return QString("%1, %2, %3")
			.arg(s->detectColor.r)
			.arg(s->detectColor.g)
			.arg(s->detectColor.b);
	}
	return QVariant();
}
