#ifndef _SABER_HPP
#define _SABER_HPP

#pragma once

#include "glapp.hpp"
#include "glw.hpp"
#include "model.hpp"

#include <opencv2/opencv.hpp>

/**
 * Renders 2D sabers.
 */
class Saber2d : public GLApp2d {
	cv::VideoCapture *camera;
	int winHeight, winWidth;
	GLuint texture_id;
public:
	Saber2d(cv::VideoCapture *camera);
	void initialize();
	void render();
	void resize(int width, int height);
	void renderSaber(Saber &saber, pos &A, int sizeSaber);
	static std::vector<cv::Vec4i>* detectSaber(cv::Mat &frameHSV, Saber *saber);
};

#endif /* _SABER_HPP */
