#include "glapp.hpp"

#include <cstring>

using std::memset;

// Vertex Shader.
static const char *vert_shader_2d = "#version 130\n"
	"attribute vec3 vx_pos;\n" // Vertice
	"attribute vec2 vx_tuv;\n" // Color
	
	"uniform int mode_i;" // coord_m4 usage.
	
	"uniform mat4  proj_m4;\n" // 2D proj matrix
	"uniform mat4 coord_m4;\n" // 2D draw coordinates
	
	"varying vec2 tex_uv;\n"
	
	"void main() {\n"
	"	if (mode_i==3) gl_Position = vec4(vx_pos, 1.0);\n"
	"	else if (mode_i==2) gl_Position = proj_m4 * coord_m4[gl_VertexID];\n"
	"	else gl_Position = proj_m4 * coord_m4 * vec4(vx_pos, 1.0);\n"
	"	tex_uv = vx_tuv;\n"
	"}";

// Fragment Shader.
static const char *frag_shader_2d = "#version 130\n"
	"precision mediump int;\n"
	"precision mediump float;\n"
	
	"uniform sampler2D texture;\n"
    "uniform vec4 blend_col_v4;\n"
	
	"varying vec2 tex_uv;\n"
	
	"void main() {\n"
	"	gl_FragColor = texture2D(texture, tex_uv) * blend_col_v4;\n"
	"}";

// Vertices of the backplane.
static const GLfloat vertices_plan_2d[4][3] = {
	{-1., -1., 0.},
	{ 1., -1., 0.},
	{ 1.,  1., 0.},
	{-1.,  1., 0.}
};

// Texture UV coordinates of the backplane.
static const GLfloat uv_plan_2d[4][2] = {
	{ 0., 0.},
	{ 1., 0.},
	{ 1., 1.},
	{ 0., 1.}
};

// Indices of the 2 triangles of the backplane.
static const GLuint indices_tri_2d[2][3] = {
	{0, 1, 3},
	{1, 2, 3}
};

void GLApp2d::initialize() {
	// GL Extension Wrangler initialization.
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		std::cerr << "ERROR: glewInit() failed, " << glewGetErrorString(err) << std::endl;
		std::exit(-1);
	}
	
	std::cout << "OpenGL ver. " << glGetString(GL_VERSION) << std::endl;
	
	// Compile, link and use shader programs.
	prog_id = glCreateProgram();
	glBindAttribLocation(prog_id, 0, "vx_pos");
	glBindAttribLocation(prog_id, 1, "vx_tuv");
	GLuint vertShader = loadShader(prog_id,   GL_VERTEX_SHADER, vert_shader_2d);
	GLuint fragShader = loadShader(prog_id, GL_FRAGMENT_SHADER, frag_shader_2d);
	linkProgram(prog_id);
	
	glDetachShader(prog_id, vertShader);
	glDetachShader(prog_id, fragShader);
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);
	
	glUseProgram(prog_id);
	glEnableVertexAttribArray(0); // enables vx_pos
	glEnableVertexAttribArray(1); // enables vx_tuv
	
	// Gen buffers to store vertices, UV coordinates and indices.
	glGenBuffers(3, buffers);
	
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	glBufferData(GL_ARRAY_BUFFER, 3*4*sizeof(GLfloat), vertices_plan_2d, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	glBufferData(GL_ARRAY_BUFFER, 2*4*sizeof(GLfloat),       uv_plan_2d, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2*3*sizeof(GLint), indices_tri_2d, GL_STATIC_DRAW);
	
	// Textures.
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	
	// Initialise the blending color.
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	blend_col_v4 = glGetUniformLocation(prog_id, "blend_col_v4");
	glUniform4f(blend_col_v4, 1., 1., 1., 1.);
	
	// Gets the coordinates transform matrix id.
	mode_i = glGetUniformLocation(prog_id, "mode_i");
	glUniform1i(mode_i, 1);
	coord_m4 = glGetUniformLocation(prog_id, "coord_m4");
	GLERRCHK;
}

void GLApp2d::resize(int width, int height) {
	if (height==0) height=1;
	if ( width==0)  width=1;
	glViewport(0, 0, (GLint)width, (GLint)height);
	
	// 2D projection matrix. Translates (0,0):(ScreenWidth,ScreenHeight) to (-1,-1)(1,1) coordinates.
	GLfloat proj_mat[4][4];
	memset(proj_mat, 0, 4*4*sizeof(float));
	proj_mat[0][0] =  2.0f /  (float)width;
	proj_mat[1][1] = -2.0f / (float)height;
	proj_mat[2][2] =  1.0f;
	proj_mat[3][0] = -1.0f;
	proj_mat[3][1] =  1.0f;
	proj_mat[3][3] =  1.0f;
	
	GLuint proj_m4 = glGetUniformLocation(prog_id, "proj_m4");
	glUniformMatrix4fv(proj_m4, 1, GL_FALSE, (GLfloat*)proj_mat);
	GLERRCHK;
}

void GLApp2d::render() {
	// Draws the backplane.
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[2]);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);	
}

void GLApp2d::cleanup() {
	glDeleteTextures(1, &texture);
	glDeleteBuffers(3, buffers);
	glDeleteProgram(prog_id);
}
