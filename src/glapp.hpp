#ifndef _GLAPP_HPP
#define _GLAPP_HPP

#pragma once

#include "glw.hpp"

/**
 * The GLApp interface(pure virtual class) contains your OpenGL application.
 */
class GLApp {
public:
	/// Called first, set up everything you need here.
	virtual void initialize() = 0;
	
	/// Called each frame.
	virtual void render() = 0;
	
	/// Called once after GLApp::initialize() and each time the window is resized.
	/// You should set the viewport and the projection matrix.
	virtual void resize(int width, int height) = 0;
	
	/// Called when this application will no longer be in use.
	virtual void cleanup() = 0;
};



/**
 * A GLApp with preimplemented methods for 2D drawing.
 */
class GLApp2d : public GLApp {
protected:
	// GL names (IDentifiers)
	/// Shaders program ID (for glUseProgram).
	GLuint prog_id;
	
	/** Buffers IDs (0=vertices  1=UV_texture_coordinates  2=indices).
	 *  You won't need these IDs. */
	GLuint buffers[3];
	
	/// Texture ID. You must set it before you call GLApp2d::render().
	GLuint texture;
	
	/// Blending color, 4 floats: R,G,B,A between [0; 1].
	GLuint blend_col_v4;
	
	/// There is 2 mode for the coord_m4 matrix :
	/// 1 - coord_m4 is a model matrix applied to the default vertices.
	/// 2 - coord_m4 contains the 4 vertices, one per column.
	GLuint mode_i;
	
	/// Coordinates matrix (2D model matrix) ID. You must set it before you call GLApp2d::render().
	/// Example :
	/// [ ImageWidth       0       X-translate ]
	/// [     0       ImageHeight  Y-translate ]
	/// [     0            0           1.0     ]
	/// May contain any linear combination of ( scale | translate | rotate ) matrices.
	GLuint coord_m4;
	
public:
	/// Loads the shaders/buffers/attributes.
	void initialize();
	
	/// Draws the current `texture` at the coordinates parameterized by the `coord_m4` matrix.
	/// The projection matrix translates (0,0):(ScreenWidth,ScreenHeight) to (-1,-1)(1,1) coordinates.
	void render();
	
	/// Sets the 2D projection matrix and the gl viewport.
	void resize(int width, int height);
	
	/// Un-initialize (deletes the shaders/buffers/texture/attributes).
	void cleanup();
};

#endif /* _GLAPP_HPP */
