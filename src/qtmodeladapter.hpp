#ifndef _QT_MODEL_AD
#define _QT_MODEL_AD

#pragma once

#include "model.hpp"

#include <QAbstractListModel>
#include <QVariant>

class QtModelAdapter: public QAbstractListModel {
public:
	int rowCount(const QModelIndex &parent) const;
	QVariant data(const QModelIndex &index, int role) const;
	// Do I have to implements the next method ?
	//QVariant headerData(int section, Qt::Orientation orientation, int role) const;
};

#endif // _QT_MODEL_AD
