#ifndef _WINDOW_HPP
#define _WINDOW_HPP

#pragma once

#include "glw.hpp"
#include "glapp.hpp"
#include "model.hpp"
#include "qtmodeladapter.hpp"

#include <QApplication>
#include <QMainWindow>
#include <QDockWidget>
#include <QPushButton>
#include <QModelIndex>
#include <QListView>
#include <QGLFormat>
#include <QGLWidget>
#include <QSpinBox>
#include <QToolBar>
#include <QWidget>
#include <QTimer>
#include <QLabel>

/**
 * Viewer is the main viewing window for this project.
 * A Viewer contains a RenderWidget as its central widget.
 */
class Viewer: public QMainWindow {
	Q_OBJECT
	
	QToolBar *toolbar;
	
	/// Saber List DockWidget.
	QDockWidget *sabersdw;
	QListView *listview;
	QtModelAdapter *adapter;
	
	/// Saber Properties DockWidget.
	QDockWidget *propsdw;
	QWidget *widget;
	QLabel *labels[2];
	QPushButton *buttons[2];
	
	/// Global Properties DockWidget.
	QDockWidget *globdw;
	QLabel *labels2[8];
	QSpinBox *values[8];
	QWidget *widget2;
	
	/// Label in the statusbar to display the FramePerSeconds of the GLWidget.
	QLabel *fpsIndicator;
	/// Timer to update the fpsIndicator.
	QTimer *fpsTimer;
	/// For FPS calculation purpose.
	long lastFrameCount;
	
private slots:
	/// Recalculates the FPS and display the new value in the status bar.
	void updateFPS();
	
	/// Setters for every fields of the Global properties DockWidget.
	void setDeltaH(int v);
	void setDeltaS(int v);
	void setDeltaV(int v);
	void setRho(int v);
	void setTheta(int v);
	void setThreshold(int v);
	void setMinLineLength(int v);
	void setMaxLineGap(int v);
	
public slots:
	/// When a saber has been added, refreshes the listview.
	void saberAdded();
	/// When a saber has been selected in the listview.
	void saberSelected(QModelIndex index);
	/// When the user wants to change the color of a saber.
	void changeColor();
	
public:
	/// Creates a Viewer if you already have a QApplication.
	explicit Viewer(GLApp *glapp);
	~Viewer();
	
	/// Creates a QApplication and start a standalone Viewer using the given GLApp.
	static int mainStart(GLApp *glapp) {
		char *str = {'\0'};
		int len = 1;
		QApplication qapp(len, &str);
		qapp.connect(&qapp, SIGNAL(lastWindowClosed()), &qapp, SLOT(quit()));

		Viewer v(glapp);
		v.show();

		return qapp.exec();
	}
};

// ---

/**
 * RenderWidget is an OpenGL render widget.
 */
class RenderWidget: public QGLWidget {
	Q_OBJECT
	/// this timer generates a signal periodicly which is connected to the RenderWidget::updateGL slot.
	QTimer *timer;
	
	/// Contains the OpenGL code to be shown in this RenderWidget.
	GLApp *glapp;
	
	/// For FPS calculation.
	unsigned long frameCount;
	
	/// 0=idle, 1=addSaber
	int state;
	
private slots:
	void addSaberMode();
	
public:
	RenderWidget(GLApp *glapp=NULL, QWidget *parent=NULL);
	~RenderWidget();
	
	/// Use a new GLApp, will call GLApp::initialize and then GLApp::resize.
	void setGLApp(GLApp *glapp);
	
	/// Sets the canvas size (used by the parent of this widget)
	QSize sizeHint() const;
	QSize minimumSizeHint() const;
	
	/// Returns how many frames have been rendered since the start.
	unsigned long getFrameCount();
	
	void mousePressEvent(QMouseEvent *event);
	
protected:
	/// Redefined QGLWidget methods :
	/// Calls GLApp::initialize.
	void initializeGL();
	/// Calls GLApp::render.
	void paintGL();
	/// Calls GLApp::resize.
	void resizeGL(int width, int height);
};

#endif /* _WINDOW_HPP */
