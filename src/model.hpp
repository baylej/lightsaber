#ifndef _MODEL_HPP
#define _MODEL_HPP

#pragma once

#include <vector>

/**
 * The model contains all the data for this project.
 */

/// The pos structure contains the position of a saber.
struct pos {
	int x1,y1,x2,y2;
};

/// The col structure contains an RGB color.
struct col {
	unsigned char r,g,b;
};

/// A saber.
struct Saber {
	struct col detectColor;
	struct col saberColor;
	struct pos saberPos;
};

/// A collection of sabers.
extern std::vector<Saber*> *sabers;

/// The global config.
struct _config {
	int deltaH, deltaS, deltaV;
	int rho, theta, threshold, minLineLength, maxLineGap;
};

extern struct _config globConfig;

#endif /* _MODEL_HPP */
