#ifndef _GLW_HPP
#define _GLW_HPP

/**
 * GL Work header, include this header instead of GL/gl.h if you need OpenGL functions.
 * Use the GL Extension Wrangler, you have to call glewInit() once in your project
 * in order to use Modern OpenGL.
 */

#include <GL/glew.h>

#include <cstdlib>
#include <iostream>

/// Exits the program if an OpenGL error occured + writes on stderr the file and the line where the error was caught.
#define GLERRCHK { int err = glGetError(); if(err != GL_NO_ERROR) { std::cerr << "Gl Error : " << err << " (" << __FILE__ << ":" << __LINE__ << ")" << std::endl; std::exit(-1); } }

/// Compiles and attaches a Shader into a program.
inline GLuint loadShader(GLuint program, GLenum shaderType, const GLchar *source) {
	// Compiles.
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	
	int compiled = 0;
	glGetShaderiv(shader, GL_OBJECT_COMPILE_STATUS_ARB, &compiled);
	if(compiled == GL_FALSE) {
		char *failStr = new char[250];
		glGetShaderInfoLog(shader, 250, NULL, failStr);
		std::cerr << failStr;
		std::exit(1);
	}
	
	// Attaches.
	glAttachShader(program, shader);
	return shader;
}

/// Links a program.
inline void linkProgram(GLuint program) {
	glLinkProgram(program);
	
	int linked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE) {
		char *failStr = new char[250];
		glGetProgramInfoLog(program, 250, NULL, failStr);
		std::cerr << failStr;
		std::exit(1);
	}
}

#endif /* _GLW_HPP */
