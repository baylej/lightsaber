#include "viewer.hpp"

#include <QLabel>
#include <QStatusBar>
#include <QTextStream>
#include <QAction>
#include <QPixmap>
#include <QColor>
#include <QFormLayout>
#include <QMouseEvent>
#include <QIcon>
#include <QColorDialog>

Viewer::Viewer(GLApp *glapp) {
	RenderWidget *glw = new RenderWidget(glapp, this);
	
	if (glw->isValid()) {
		setCentralWidget(glw);
	} else {
		setCentralWidget(new QLabel(tr("ERROR: Failed to create an OpenGL context."), this));
	}
	setWindowTitle(tr("Lightsaber"));
	
	// Saber List DockWidget
	sabersdw = new QDockWidget(tr("Sabers"), this);
	sabersdw->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	adapter = new QtModelAdapter();
	listview = new QListView(sabersdw);
	listview->setMinimumWidth(200);
	listview->setModel(adapter);
	connect(listview, SIGNAL(pressed(QModelIndex)), this, SLOT(saberSelected(QModelIndex)));
	sabersdw->setWidget(listview);
	addDockWidget(Qt::RightDockWidgetArea, sabersdw, Qt::Vertical);
	
	// Saber Properties DockWidget
	propsdw = new QDockWidget(tr("Saber Properties"), this);
	propsdw->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	labels[0] = new QLabel(tr("Detect color"));
	labels[1] = new QLabel(tr("Saber color"));
	QPixmap pixmap(16, 16);
	QColor color(Qt::white);
	pixmap.fill(color);
	QIcon icon(pixmap);
	buttons[0] = new QPushButton(icon, NULL);
	buttons[1] = new QPushButton(icon, NULL);
	connect(buttons[1], SIGNAL(clicked()), this, SLOT(changeColor()));
	QFormLayout *layout = new QFormLayout();
	layout->addRow(labels[0], buttons[0]);
	layout->addRow(labels[1], buttons[1]);
	widget = new QWidget(propsdw);
	widget->setLayout(layout);
	propsdw->setWidget(widget);
	addDockWidget(Qt::RightDockWidgetArea, propsdw, Qt::Vertical);
	
	// Global properties DockWidget
	globdw = new QDockWidget(tr("Global Properties"), this);
	globdw->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	labels2[0] = new QLabel(tr("delta H"));
	labels2[1] = new QLabel(tr("delta S"));
	labels2[2] = new QLabel(tr("delta V"));
	labels2[3] = new QLabel(tr("rho"));
	labels2[4] = new QLabel(tr("theta PI/"));
	labels2[5] = new QLabel(tr("threshold"));
	labels2[6] = new QLabel(tr("minLineLength"));
	labels2[7] = new QLabel(tr("maxLineGap"));
	values[0] = new QSpinBox();  values[0]->setRange(0,  50);  values[0]->setValue(globConfig.deltaH);
	values[1] = new QSpinBox();  values[1]->setRange(0, 128);  values[1]->setValue(globConfig.deltaS);
	values[2] = new QSpinBox();  values[2]->setRange(0, 128);  values[2]->setValue(globConfig.deltaV);
	values[3] = new QSpinBox();  values[3]->setRange(1, 200);  values[3]->setValue(globConfig.rho);
	values[4] = new QSpinBox();  values[4]->setRange(6, 180);  values[4]->setValue(globConfig.theta);
	values[5] = new QSpinBox();  values[5]->setRange(0, 500);  values[5]->setValue(globConfig.threshold);
	values[6] = new QSpinBox();  values[6]->setRange(0, 300);  values[6]->setValue(globConfig.minLineLength);
	values[7] = new QSpinBox();  values[7]->setRange(0, 150);  values[7]->setValue(globConfig.maxLineGap);
	connect(values[0], SIGNAL(valueChanged(int)), this, SLOT(setDeltaH(int)));
	connect(values[1], SIGNAL(valueChanged(int)), this, SLOT(setDeltaS(int)));
	connect(values[2], SIGNAL(valueChanged(int)), this, SLOT(setDeltaV(int)));
	connect(values[3], SIGNAL(valueChanged(int)), this, SLOT(setRho(int)));
	connect(values[4], SIGNAL(valueChanged(int)), this, SLOT(setTheta(int)));
	connect(values[5], SIGNAL(valueChanged(int)), this, SLOT(setThreshold(int)));
	connect(values[6], SIGNAL(valueChanged(int)), this, SLOT(setMinLineLength(int)));
	connect(values[7], SIGNAL(valueChanged(int)), this, SLOT(setMaxLineGap(int)));
	layout = new QFormLayout();
	for (int i=0; i<8; i++) {
		layout->addRow(labels2[i], values[i]);
	}
	widget2 = new QWidget(globdw);
	widget2->setLayout(layout);
	globdw->setWidget(widget2);
	addDockWidget(Qt::RightDockWidgetArea, globdw, Qt::Vertical);
	
	// StatusBar
	QStatusBar *statusBar = new QStatusBar();
	fpsIndicator = new QLabel(statusBar);
	statusBar->addPermanentWidget(fpsIndicator);
	setStatusBar(statusBar);
	
	lastFrameCount = 0l;
	fpsTimer = new QTimer(this);
	connect(fpsTimer, SIGNAL(timeout()), this, SLOT(updateFPS()));
	fpsTimer->start(1000); // Timeouts every 1sec.
	
	// ToolBar
	toolbar = new QToolBar();
	toolbar->setMovable(false);
	QAction *action = toolbar->addAction(tr("Add a saber"));
	connect(action, SIGNAL(triggered()), glw, SLOT(addSaberMode()));
	addToolBar(Qt::TopToolBarArea, toolbar);
}

Viewer::~Viewer() {
	delete this->centralWidget();
	delete fpsTimer;
	delete fpsIndicator;
	delete statusBar();
	delete toolbar;
	delete listview;
	delete adapter;
	delete sabersdw;
	delete labels[0];  delete labels[1];
	delete buttons[0]; delete buttons[1];
	delete widget;
	delete propsdw;
	for (int i=0; i<8; i++) {
		delete labels2[i];
		delete values[i];
	}
	delete widget2;
	delete globdw;
}

void Viewer::updateFPS() {
	unsigned long currFC = static_cast<RenderWidget*>(this->centralWidget())->getFrameCount();
	// Assumes this slot is called every 1sec.
	QString text;
	QTextStream(&text) << "FPS=" << currFC - lastFrameCount;
	fpsIndicator->setText(text);
	lastFrameCount = currFC;
}

void Viewer::setDeltaH(int v) {
	globConfig.deltaH = v;
}

void Viewer::setDeltaS(int v) {
	globConfig.deltaS = v;
}

void Viewer::setDeltaV(int v) {
	globConfig.deltaV = v;
}

void Viewer::setRho(int v) {
	globConfig.rho = v;
}

void Viewer::setTheta(int v) {
	globConfig.theta = v;
}

void Viewer::setThreshold(int v) {
	globConfig.threshold = v;
}

void Viewer::setMinLineLength(int v) {
	globConfig.minLineLength = v;
}

void Viewer::setMaxLineGap(int v) {
	globConfig.maxLineGap = v;
}

void Viewer::saberAdded() {
	// Resets the listview, not the most efficient way, but there won't be more than 3 sabers.
	listview->reset();
}

void Viewer::saberSelected(QModelIndex index) {
	Saber *s = (*sabers)[index.row()];
	// Changes the colors of the buttons.
	QPixmap pixmap(16, 16);
	QColor color(s->detectColor.r, s->detectColor.g, s->detectColor.b);
	pixmap.fill(color);
	QIcon icon1(pixmap);
	color.setRgb(s->saberColor.r, s->saberColor.g, s->saberColor.b);
	pixmap.fill(color);
	QIcon icon2(pixmap);
	buttons[0]->setIcon(icon1);
	buttons[1]->setIcon(icon2);
}

void Viewer::changeColor() {
	Saber *s = (*sabers)[listview->currentIndex().row()];
	QColorDialog dialog(this);
	QColor res = dialog.getColor(QColor(s->saberColor.r, s->saberColor.g, s->saberColor.b));
	s->saberColor.r = res.red();
	s->saberColor.g = res.green();
	s->saberColor.b = res.blue();
	// Changes the color of the button.
	QPixmap pixmap(16, 16);
	QColor color(s->saberColor.r, s->saberColor.g, s->saberColor.b);
	pixmap.fill(color);
	QIcon icon(pixmap);
	buttons[1]->setIcon(icon);
}

// ---

static const int framerate = 30; // Frames per second.

RenderWidget::RenderWidget(GLApp *glapp, QWidget *parent) : QGLWidget(parent) {
	setFocusPolicy(Qt::ClickFocus);
	
	this->glapp = glapp;
	
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()),this, SLOT(updateGL()));
	timer->start(1000 / framerate);
	
	frameCount = 0l;
	state = 0;
}

RenderWidget::~RenderWidget() {
	delete this->timer;
}

void RenderWidget::addSaberMode() {
	setCursor(Qt::CrossCursor);
	state = 1;
}

void RenderWidget::setGLApp(GLApp *glapp) {
	if (glapp) {
		this->timer->stop();
		if (this->glapp) {
			this->glapp->cleanup();
		}
		this->glapp = glapp;
		if (this->glapp) {
			this->glapp->initialize();
			this->glapp->resize(this->width(), this->height());
		}
		this->timer->start(1000 / framerate);
	}
}

QSize RenderWidget::sizeHint() const {
	QSize size(800, 600);
	return size;
}

QSize RenderWidget::minimumSizeHint() const {
    return sizeHint();
}

unsigned long RenderWidget::getFrameCount() {
	return frameCount;
}

void RenderWidget::paintGL() {
	if (this->glapp) {
		this->glapp->render();
		frameCount++;
	}
}

void RenderWidget::resizeGL(int width, int height) {
	if (this->glapp) {
		this->glapp->resize(width, height);
	}
}

void RenderWidget::initializeGL() {
	if (this->glapp) {
		this->glapp->initialize();
	}
}

void RenderWidget::mousePressEvent(QMouseEvent *event) {
	if (state == 1) {
		GLint x = event->x(), y = height() - event->y();
		GLsizei w = 1, h = 1;
		unsigned char rgb[3];
		
		// Reads the pixel at the mouse location in the frame buffer.
		glReadPixels(x, y, w, h, GL_RGB, GL_UNSIGNED_BYTE, rgb);
		GLERRCHK;
		
		// Adds a Saber replacing a stick of this color.
		Saber *s = new Saber;
		s->detectColor.r = rgb[0];
		s->detectColor.g = rgb[1];
		s->detectColor.b = rgb[2];
		sabers->push_back(s);
		
		static_cast<Viewer*>(parent())->saberAdded();
		
		state = 0;
		setCursor(Qt::ArrowCursor);
	}
}

