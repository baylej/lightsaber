# Lightsaber

Lightsaber uses [OpenCV](http://opencv.org), Qt4 and OpenGL.

## Licence ##

Copyright (C) 2015  Bardoux, Bayle

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

## Dependencies

OpenCV, Qt4, Qt4-OpenGL, OpenGL3.3+, glew.

On Debian/Ubuntu :

```bash
apt-get install build-essential cmake cmake-curses-gui
apt-get install libopencv libopencv-dev libglew1.10 libglew-dev libqt4-dev libqt4-dev-bin libqt4-core libqt4-gui libqt4-opengl libqt4-opengl-dev
```

## Building

```bash
git clone git@bitbucket.org:baylej/lightsaber.git
cd lightsaber
mkdir build; cd build
cmake ..
make && make install
```

## Hacking

I advise you to use QtCreator, you will need its cmake plugin in order to import the CMakeList.txt file as a project.

On Debian/Ubuntu :

```bash
apt-get install qt4-dev-tools qt4-doc qt4-doc-html qtcreator qtcreator-doc qtcreator-plugin-cmake
```

Otherwise you may use your favourite text editor and a terminal.